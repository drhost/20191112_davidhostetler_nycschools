package com.example.a20191112_davidhostetler_nycschools;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.model.SatData;
import com.example.a20191112_davidhostetler_nycschools.view_model.HighSchoolDetailViewModel;

import java.util.List;

public class HighSchoolDetailActivity extends AppCompatActivity {
    public static final String INTENT_EXTRA_DETAILS = "details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_school_details);
        final HighSchool highSchool = getIntent().getParcelableExtra(INTENT_EXTRA_DETAILS);
        ((TextView) findViewById(R.id.name)).setText(highSchool.schoolName);
        ((TextView) findViewById(R.id.address)).setText(highSchool.address);
        ((TextView) findViewById(R.id.phone)).setText(highSchool.phone);
        ((TextView) findViewById(R.id.fax)).setText(highSchool.fax);
        ((TextView) findViewById(R.id.email)).setText(highSchool.email);
        ((TextView) findViewById(R.id.website)).setText(highSchool.website);

        final HighSchoolDetailViewModel highSchoolDetailViewModel = ViewModelProviders.of(this).get(HighSchoolDetailViewModel.class);
        highSchoolDetailViewModel.fetchSatData(highSchool.databaseId);
        highSchoolDetailViewModel.mSatData.observe(this, new Observer<List<SatData>>() {
            @Override
            public void onChanged(@Nullable List<SatData> satData) {
                ((TextView) findViewById(R.id.mathAverageScore)).setText("" + satData.get(0).mathScore);
                ((TextView) findViewById(R.id.readingAverageScore)).setText("" + satData.get(0).readingScore);
                ((TextView) findViewById(R.id.writingAverageScore)).setText("" + satData.get(0).writingScore);
            }
        });

        highSchoolDetailViewModel.mDataPreset.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean dataPresent) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                if (!dataPresent) {
                    findViewById(R.id.empty).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.mathLabel).setVisibility(View.VISIBLE);
                    findViewById(R.id.mathAverageScore).setVisibility(View.VISIBLE);
                    findViewById(R.id.readingLabel).setVisibility(View.VISIBLE);
                    findViewById(R.id.readingAverageScore).setVisibility(View.VISIBLE);
                    findViewById(R.id.writingLabel).setVisibility(View.VISIBLE);
                    findViewById(R.id.writingAverageScore).setVisibility(View.VISIBLE);
                }
            }
        });
    }

}
