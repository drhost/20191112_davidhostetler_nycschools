package com.example.a20191112_davidhostetler_nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SatData implements Parcelable {
    @SerializedName("dbn")
    public String databaseId;

    @SerializedName("sat_critical_reading_avg_score")
    public int readingScore;

    @SerializedName("sat_math_avg_score")
    public int mathScore;

    @SerializedName("sat_writing_avg_score")
    public int writingScore;

    public SatData() {}
    public SatData(Parcel in) {
        databaseId = in.readString();
        readingScore = in.readInt();
        mathScore = in.readInt();
        writingScore = in.readInt();
    }

    public static final Parcelable.Creator<SatData> CREATOR = new Parcelable.Creator<SatData>() {
        @Override
        public SatData createFromParcel(Parcel in) {
            return new SatData(in);
        }

        @Override
        public SatData[] newArray(int size) {
            return new SatData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(databaseId);
        dest.writeInt(readingScore);
        dest.writeInt(mathScore);
        dest.writeInt(writingScore);
    }
}

/*
https://data.cityofnewyork.us/resource/f9bf-2cp4.json?$where=dbn=%2701M448%27
[
{
"dbn": "01M448",
"school_name": "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL",
"num_of_sat_test_takers": "91",
"sat_critical_reading_avg_score": "383",
"sat_math_avg_score": "423",
"sat_writing_avg_score": "366"
}
]
*
*/
