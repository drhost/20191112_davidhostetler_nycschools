package com.example.a20191112_davidhostetler_nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class HighSchool implements Parcelable {
    @SerializedName("dbn")
    public String databaseId;

    @SerializedName("school_name")
    public String schoolName;

    @SerializedName("location")
    public String address;

    @SerializedName("phone_number")
    public String phone;

    @SerializedName("fax_number")
    public String fax;

    @SerializedName("school_email")
    public String email;

    @SerializedName("website")
    public String website;

    public HighSchool() {}
    public HighSchool(Parcel in) {
        databaseId = in.readString();
        schoolName = in.readString();
        address = in.readString();
        phone = in.readString();
        fax = in.readString();
        email = in.readString();
        website = in.readString();
    }

    public static final Parcelable.Creator<HighSchool> CREATOR = new Parcelable.Creator<HighSchool>() {
        @Override
        public HighSchool createFromParcel(Parcel in) {
            return new HighSchool(in);
        }

        @Override
        public HighSchool[] newArray(int size) {
            return new HighSchool[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(databaseId);
        dest.writeString(schoolName);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(fax);
        dest.writeString(email);
        dest.writeString(website);
    }
}