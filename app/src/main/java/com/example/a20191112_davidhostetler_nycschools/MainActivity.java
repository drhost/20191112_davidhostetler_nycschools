package com.example.a20191112_davidhostetler_nycschools;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.os.Bundle;

import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.view_model.HighSchoolViewModel;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private HighSchoolViewModel highSchoolViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        highSchoolViewModel = ViewModelProviders.of(this).get(HighSchoolViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.schoolRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new HighScrollAdapter(highSchoolViewModel));
        recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                highSchoolViewModel.handlePagination( ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition());
            }
        });

        highSchoolViewModel.fetchHighSchoolData(0);

        highSchoolViewModel.mHighScrollList.observe(this, new Observer<ArrayList<HighSchool>>() {
            @Override
            public void onChanged(@Nullable ArrayList<HighSchool> highSchools) {
                ((HighScrollAdapter) recyclerView.getAdapter()).setHighSchools(highSchools);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });

        highSchoolViewModel.mDataPreset.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean dataPresent) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                if (!dataPresent) {
                    findViewById(R.id.empty).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.schoolRecyclerView).setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        highSchoolViewModel.onRotationChange();
    }

}
