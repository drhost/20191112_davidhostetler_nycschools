package com.example.a20191112_davidhostetler_nycschools.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;

import com.example.a20191112_davidhostetler_nycschools.HighSchoolDetailActivity;
import com.example.a20191112_davidhostetler_nycschools.R;
import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.network.BaseRetrofitRequest;
import com.example.a20191112_davidhostetler_nycschools.network.GetHighSchoolsAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HighSchoolViewModel extends ViewModel {
    public static final int PAGE_LIMIT = 20;
    public static final String ORDER_BY_SCHOOL_NAME = "school_name ASC";

    public MutableLiveData<ArrayList<HighSchool>> mHighScrollList = new MutableLiveData<>();
    public MutableLiveData<Boolean> mDataPreset = new MutableLiveData<>();

    // This boolean is used to prevent multiple requests for the same page.
    private boolean mPaginationComplete = true;

    public HighSchoolViewModel() {
        super();
    }

    public void onRotationChange() {
        mHighScrollList.setValue(null);
        fetchHighSchoolData(0);
    }

    public void onItemClick(final View view, int position) {
        final Intent intent = new Intent(view.getContext(), HighSchoolDetailActivity.class);
        HighSchool highSchool = mHighScrollList.getValue().get(position);
        // Strip off any location values on the address
        highSchool.address =  highSchool.address.indexOf("(") != -1 ?
                highSchool.address.substring(0, highSchool.address.indexOf("(")) :
                highSchool.address;

        // Validating if any fields are not present and replacing any null value with an "Unavailable" statement.
        // For phones, I am replacing the format with a more common US format, i.e. (408) 444-1111.
        if(highSchool.phone == null) {
            highSchool.phone = view.getContext().getResources().getString(R.string.no_phone_available);
        } else {
            highSchool.phone = PhoneNumberUtils.formatNumber(highSchool.phone, Locale.getDefault().getCountry());
        }

        if(highSchool.fax == null) {
            highSchool.fax = view.getContext().getResources().getString(R.string.no_fax_available);
        } else {
            highSchool.fax = PhoneNumberUtils.formatNumber(highSchool.fax, Locale.getDefault().getCountry());
        }

        if(highSchool.email == null) {
            highSchool.email = view.getContext().getResources().getString(R.string.no_email_available);
        }

        if(highSchool.website == null) {
            highSchool.website = view.getContext().getResources().getString(R.string.no_website_available);
        }

        intent.putExtra(HighSchoolDetailActivity.INTENT_EXTRA_DETAILS, mHighScrollList.getValue().get(position));
        view.getContext().startActivity(intent);
    }

    public void handlePagination(int position) {
        // The only reason this can happen is if the recycler view reports a scroll event during rotation.
        if (mHighScrollList.getValue() == null) {
            return;
        }
        int currentListSize = mHighScrollList.getValue().size();
        // Only continue with pagination in the case where a full set of schools were returned in the last request.
        if (currentListSize % PAGE_LIMIT != 0) {
            return;
        }

        // Get more schools when the user scrolls past the mid point of the current amount of schools.
        if (position > (currentListSize / 2) && mPaginationComplete) {
            mPaginationComplete = false;
            fetchHighSchoolData(currentListSize+1);
        }
    }

    public void fetchHighSchoolData(final int offset) {
        // I have used the ordering feature of the NYC Open Data to display the schools in an sorted ascending order.
        BaseRetrofitRequest.getInstance().create(GetHighSchoolsAPI.class).getHighSchools(PAGE_LIMIT, offset, ORDER_BY_SCHOOL_NAME).enqueue(new Callback<List<HighSchool>>() {
            @Override
            public void onResponse(Call<List<HighSchool>> call, Response<List<HighSchool>> response) {
                mPaginationComplete = true;
                ArrayList<HighSchool> highSchools = mHighScrollList.getValue();
                if (response.body().size() > 0) {
                    if (highSchools == null) {
                        highSchools = new ArrayList<>();
                    }

                    highSchools.addAll(response.body());
                    mHighScrollList.setValue(highSchools);
                    mDataPreset.setValue(true);
                } else if (highSchools == null){
                    // Only ask to display the empty view if there were no high schools.  Do not display empty view for the case
                    // where no more high schools are returned for the next page.
                    mDataPreset.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<HighSchool>> call, Throwable t) {
                Log.e(HighSchoolViewModel.class.getSimpleName(), "Error retrieve data: " + t.getMessage());
                mDataPreset.setValue(false);
                mPaginationComplete = true;
            }
        });
    }
}