package com.example.a20191112_davidhostetler_nycschools.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.example.a20191112_davidhostetler_nycschools.HighSchoolDetailActivity;
import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.model.SatData;
import com.example.a20191112_davidhostetler_nycschools.network.BaseRetrofitRequest;
import com.example.a20191112_davidhostetler_nycschools.network.GetHighSchoolsAPI;
import com.example.a20191112_davidhostetler_nycschools.network.GetSatScoreAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HighSchoolDetailViewModel extends ViewModel {
    public static final String DATABASE_ID_KEYWORD = "dbn";

    public MutableLiveData<List<SatData>> mSatData = new MutableLiveData<>();
    public MutableLiveData<Boolean> mDataPreset = new MutableLiveData<>();

    public HighSchoolDetailViewModel() {
        super();
    }

    public void fetchSatData(final String databaseId) {
        // The following creates the where condition used by the NYC Open Data backend in order to only return
        // the specified school.
        String databaseIdSearchValue = DATABASE_ID_KEYWORD + "='" + databaseId + "'";
        BaseRetrofitRequest.getInstance().create(GetSatScoreAPI.class).getSatScores(databaseIdSearchValue).enqueue(new Callback<List<SatData>>() {
            @Override
            public void onResponse(Call<List<SatData>> call, Response<List<SatData>> response) {
                if (response.body().size() > 0) {
                    mSatData.setValue(response.body());
                    mDataPreset.setValue(true);
                } else {
                    mDataPreset.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<SatData>> call, Throwable t) {
                Log.e(HighSchoolDetailViewModel.class.getSimpleName(), "Error retrieve data: " + t.getMessage());
                mDataPreset.setValue(false);
            }
        });
    }
}