package com.example.a20191112_davidhostetler_nycschools.network;

import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.model.SatData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetSatScoreAPI {
    @GET("f9bf-2cp4")
    Call<List<SatData>> getSatScores(@Query("$WHERE") String dbnCheck);
}