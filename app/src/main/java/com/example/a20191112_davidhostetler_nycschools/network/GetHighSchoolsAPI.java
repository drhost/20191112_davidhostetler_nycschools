package com.example.a20191112_davidhostetler_nycschools.network;

import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetHighSchoolsAPI {
    @GET("s3k6-pzi2")
    Call<List<HighSchool>> getHighSchools(@Query("$LIMIT") int limit, @Query("$OFFSET") int offset, @Query("$ORDER") String order);
}