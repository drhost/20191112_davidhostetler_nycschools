package com.example.a20191112_davidhostetler_nycschools.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRetrofitRequest {
    public static final int API_TIMEOUT_IN_MINUTES = 3;
    public static final String BASE_ASSETS_URL = "https://data.cityofnewyork.us/resource/";

    private static Retrofit mRetrofit = null;

    public static Retrofit getInstance() {
        if (mRetrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(API_TIMEOUT_IN_MINUTES, TimeUnit.MINUTES);

            mRetrofit =  new Retrofit.Builder().
                    baseUrl(BASE_ASSETS_URL).
                    addConverterFactory(GsonConverterFactory.create()).
                    client((builder.build())).
                    build();
        }

        return mRetrofit;
    }
}
