package com.example.a20191112_davidhostetler_nycschools;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a20191112_davidhostetler_nycschools.model.HighSchool;
import com.example.a20191112_davidhostetler_nycschools.view_model.HighSchoolViewModel;

import java.util.ArrayList;
import java.util.List;

public class HighScrollAdapter extends RecyclerView.Adapter<HighScrollAdapter.HighSchoolViewHolder> {
    private ArrayList<HighSchool> highSchools = new ArrayList<>();
    private HighSchoolViewModel highSchoolViewModel;

    public HighScrollAdapter(final HighSchoolViewModel highSchoolViewModel) {
        this.highSchoolViewModel = highSchoolViewModel;
    }

    @Override
    public HighSchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HighSchoolViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.high_school_item, parent, false));
    }

    @Override
    public void onBindViewHolder(HighSchoolViewHolder holder, final int position) {
        final HighSchool highSchool = highSchools.get(position);
        holder.nameView.setText(highSchool.schoolName);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                highSchoolViewModel.onItemClick(view, position);
            }
        });
   }

    @Override
    public int getItemCount() {
        return highSchools == null ? 0 : highSchools.size();
    }

    public void setHighSchools(ArrayList<HighSchool> highSchools) {
        this.highSchools = highSchools;
    }

    class HighSchoolViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout itemLayout;
        protected TextView nameView;

        public HighSchoolViewHolder(View view) {
            super(view);
            itemLayout = view.findViewById(R.id.highSchoolItemLayout);
            nameView = view.findViewById(R.id.highSchoolName);
        }
    }
}
